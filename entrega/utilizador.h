#ifndef UTILIZADOR_H
#define	UTILIZADOR_H


struct utilizador{
    int pid_cliente;
    double saldo;
    int ultimo_pid;
    unsigned long ultima_contagem_clock;
};

typedef struct utilizador UTILIZADOR;
typedef struct utilizador *AP_UTILIZADOR;

#endif	/* UTILIZADOR_H */

