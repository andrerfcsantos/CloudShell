#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "utilizador.h"
#include "avl.h"
#include "headers/tamanhos_maximos.h"
#include "EstruturasPipes/pedido_servidor.h"
#include "EstruturasPipes/resposta_servidor.h"


ARVORE avl_utilizadores = NULL;


static AP_UTILIZADOR criaUtilizador(int pid);
static void pede_forma_inicializar();
static void inicializacao_pipes_sinais_arvore();
static void responde_pedidos_servidor();
static void print_arvore();
static void le_backup();
static void backup_dados(int sig);
static void cria_pipes_servidor();
static int comparaUtilizadores(const void *avl_a, const void *avl_b, void *avl_param);
static unsigned long getClock(pid_t pid);

int main(int argc, char** argv) {
    inicializacao_pipes_sinais_arvore();
    pede_forma_inicializar();
    alarm(10);
    while (1) responde_pedidos_servidor();
    return (EXIT_SUCCESS);
}

void responde_pedidos_servidor() {
    RESPOSTA_SERVIDOR resposta;
    PEDIDO_SERVIDOR pedido;
    AP_UTILIZADOR user;
    AP_UTILIZADOR user_pesquisa;
    int retorno_read, retorno_write;
    int fd_leitura, fd_escrita;
    unsigned long clock, diferenca;
    double saldo_a_descontar;
    char path_pipe_resposta_shell[MAX_PATH_PIPE];

    fd_leitura = open("Pipes/pedido_contabilizacao", O_RDONLY);

    if ((retorno_read = read(fd_leitura, &pedido, sizeof (PEDIDO_SERVIDOR))) > 0) {
        printf("[ServidorConta] Recebi o pedido com sucesso. "
                "{pid_cliente = %d  pid_exec = %d accao= %d saldo = %f}\n",
                pedido.pid_cliente, pedido.pid_exec, pedido.tipo_pedido, pedido.saldo);


        user_pesquisa = criaUtilizador(pedido.pid_cliente);
        user = avl_find(avl_utilizadores, user_pesquisa);

        if (user == NULL) {

            user = criaUtilizador(pedido.pid_cliente);
            avl_insert(avl_utilizadores, user);
        }
        free(user_pesquisa);
        switch (pedido.tipo_pedido) {
            case EXECUTE:

                clock = getClock(pedido.pid_exec);
                diferenca = (pedido.pid_exec == user->ultimo_pid) ? clock - user->ultima_contagem_clock : clock;
                user->ultima_contagem_clock = clock;
                user->ultimo_pid = pedido.pid_exec;

                saldo_a_descontar = (double) diferenca / 100;

                if (user->saldo >= saldo_a_descontar) {
                    user->saldo -= saldo_a_descontar;
                    resposta.tipo_resposta = OK;
                } else {
                    resposta.tipo_resposta = NOT_OK;
                }
                resposta.saldo_restante = user->saldo;
                sprintf(path_pipe_resposta_shell, "Pipes/%d", pedido.pid_cliente);
                fd_escrita = open(path_pipe_resposta_shell, O_WRONLY);
                break;
            case VERIFICA_SALDO:
                resposta.tipo_resposta = OK;
                resposta.saldo_restante = user->saldo;
                sprintf(path_pipe_resposta_shell, "Pipes/resposta_verificasaldo_%d", pedido.pid_cliente);
                fd_escrita = open(path_pipe_resposta_shell, O_WRONLY);
                break;
            case ADD_SALDO:
                if (user->saldo + pedido.saldo > 0) {
                    resposta.tipo_resposta = OK;
                    user->saldo += pedido.saldo;
                } else {
                    resposta.tipo_resposta = NOT_OK;
                }
                resposta.saldo_restante = user->saldo;
                sprintf(path_pipe_resposta_shell, "Pipes/resposta_addsaldo_%d", pedido.pid_cliente);
                fd_escrita = open(path_pipe_resposta_shell, O_WRONLY);
                break;
            case ADD_USER:
                user = criaUtilizador(pedido.pid_cliente);
                avl_insert(avl_utilizadores, user);
                resposta.saldo_restante = user->saldo;
                resposta.tipo_resposta = OK;
                sprintf(path_pipe_resposta_shell, "Pipes/resposta_adduser_%d", pedido.pid_cliente);
                fd_escrita = open(path_pipe_resposta_shell, O_WRONLY);
                break;
            default:
                sprintf(path_pipe_resposta_shell, "Pipes/%d", pedido.pid_cliente);
                fd_escrita = open(path_pipe_resposta_shell, O_WRONLY);
                break;
        }

        if ((retorno_write = write(fd_escrita, &resposta, sizeof (RESPOSTA_SERVIDOR))) > 0) {
            printf("[ServidorConta] Enviei a resposta com sucesso. "
                    "{saldo restante = %f resposta = %d}\n",
                    resposta.saldo_restante, resposta.tipo_resposta);
        } else {
            printf("[ServidorConta] Erro na escrita da resposta.%s\n", strerror(errno));
        }

    } else {
        if(retorno_read != 0)
        printf("[ServidorConta] Erro na leitura de pedido.retorno_read: %d %s\n", retorno_read, strerror(errno));
    }

    close(fd_leitura);
    close(fd_escrita);
}

static void inicializacao_pipes_sinais_arvore() {
    cria_pipes_servidor();
    signal(SIGALRM, backup_dados);
    signal(SIGINT, backup_dados);
    avl_utilizadores = avl_create(comparaUtilizadores, NULL, NULL);
}

static void pede_forma_inicializar() {
    int escolha;
    char linha[MAX_LINHA_BASH], *token, *delimitadores = "\n\r";
    bool escolha_ok = false;

    while (!escolha_ok) {
        printf("[ServidorConta] Bem vindo ao Servidor Contabilização\n");
        printf("[ServidorConta] Deseja iniciar o server do zero, ou recarregar dados de log.txt?\n");
        printf("[ServidorConta]       1 - Iniciar do zero\n");
        printf("[ServidorConta]       2 - Iniciar a partir de log.txt\n");
        printf("[ServidorConta]   Escolha uma opção: ");

        if (fgets(linha, MAX_LINHA_BASH, stdin) == NULL) {
            printf("A opção que escolheu não foi válida, tente novamente.\n");
        } else {
            token = strtok(linha, delimitadores);
            escolha = atoi(token);
            if (escolha == 1 || escolha == 2) {
                escolha_ok = true;
            } else {
                escolha_ok = false;
            }
        }
    }

    printf("Servidor iniciado. À espera de pedidos...\n");
    if (escolha == 2) le_backup();

}

static void le_backup() {
    char *token, *delimitadores = ":\n\r";
    AP_UTILIZADOR user;
    char *linha = (char *) malloc(sizeof (char *)*MAX_LINHA_BACKUP);
    FILE *ficheiro = fopen("log.txt", "r");

    if (ficheiro == NULL) {
        printf("[ServidorConta] Erro ao ler log.txt\n");
        return;
    }

    while (fgets(linha, MAX_LINHA_BACKUP, ficheiro) != NULL) {
        user = (AP_UTILIZADOR) malloc(sizeof (UTILIZADOR));
        token = strtok(linha, delimitadores);
        user->pid_cliente = atoi(token);
        token = strtok(NULL, delimitadores);
        user->saldo = atof(token);
        token = strtok(NULL, delimitadores);
        user->ultimo_pid = atoi(token);
        token = strtok(NULL, delimitadores);
        user->ultima_contagem_clock = strtoul(token, NULL, 10);
        avl_insert(avl_utilizadores, user);

    }

    free(linha);
}

static void print_arvore() {
    TRAVERSER iterador = avl_t_alloc();
    AP_UTILIZADOR u;
    avl_t_init(iterador, avl_utilizadores);
    FILE *ficheiro = fopen("log.txt", "r");

    while ((u = avl_t_next(iterador)) != NULL) {
        printf("%d:%f:%d:%lu\n", u->pid_cliente, u->saldo, u->ultimo_pid, u->ultima_contagem_clock);
    }
    fclose(ficheiro);
}

static void backup_dados(int sig) {
    TRAVERSER iterador = avl_t_alloc();
    AP_UTILIZADOR u;
    avl_t_init(iterador, avl_utilizadores);
    FILE *ficheiro = fopen("log.txt", "w");

    while ((u = avl_t_next(iterador)) != NULL) {
        fprintf(ficheiro, "%d:%f:%d:%lu\n", u->pid_cliente, u->saldo, u->ultimo_pid, u->ultima_contagem_clock);
    }

    fclose(ficheiro);
    printf("[ServidorConta] Backup guardado\n");
    if (sig == SIGINT) {
        printf("[ServidorConta] A matar processo...\n");
        kill(getpid(), SIGKILL);
    }
    alarm(10);
}

void cria_pipes_servidor() {
    int retorno_mkfifo;
    char *path_pipe_pedido_contabilizacao = "Pipes/pedido_contabilizacao";
    char *path_pipe_resposta_contabilizacao = "Pipes/resposta_contabilizacao";

    retorno_mkfifo = mkfifo(path_pipe_pedido_contabilizacao, 0666);
    if (retorno_mkfifo == -1)
        printf("Pipe \"%s\" não criado. %s\n", path_pipe_pedido_contabilizacao, strerror(errno));

    retorno_mkfifo = mkfifo(path_pipe_resposta_contabilizacao, 0666);
    if (retorno_mkfifo == -1)
        printf("Pipe \"%s\" não criado. %s\n", path_pipe_resposta_contabilizacao, strerror(errno));

}

static unsigned long getClock(pid_t pid) {
    int i = 1;
    unsigned long resultado;
    int retorno_open;
    char *clock_user, *ptr;
    char *delims = " \n\r\0", *token;
    char *path_proc = (char *) malloc(sizeof (char)*TAM_MAX_PATH_PROC);
    char *linha = (char *) malloc(sizeof (char)*TAM_MAX_LINHA_PROC);

    sprintf(path_proc, "/proc/%d/stat", pid);
    retorno_open = open(path_proc, O_RDONLY);

    if (retorno_open == -1) {
        printf("[ServidorConta] Erro ao abrir ficheiro proc. %s\n", strerror(errno));
        return 0;
    }

    if (read(retorno_open, linha, TAM_MAX_LINHA_PROC) == -1) {
        printf("[ServidorConta] Erro ao fazer read da linha do ficheiro proc.\n");
        return 0;
    }
    token = strtok(linha, delims);
    i++;

    while (i <= 15) {
        token = strtok(NULL, delims);
        if (i == 14) clock_user = token;
        i++;
    }

    resultado = strtoul(clock_user, &ptr, 10);

    free(path_proc);
    free(linha);
    close(retorno_open);

    return resultado;
}

static AP_UTILIZADOR criaUtilizador(int pid) {
    AP_UTILIZADOR utilizador = (AP_UTILIZADOR) malloc(sizeof (UTILIZADOR));
    utilizador->pid_cliente = pid;
    utilizador->saldo = 10;
    utilizador->ultima_contagem_clock = 0;
    utilizador->ultimo_pid = -1;
    return utilizador;
}

static int comparaUtilizadores(const void *avl_a, const void *avl_b,
        void *avl_param) {
    AP_UTILIZADOR u1 = (AP_UTILIZADOR) avl_a;
    AP_UTILIZADOR u2 = (AP_UTILIZADOR) avl_b;

    return u1->pid_cliente - u2->pid_cliente;
}
