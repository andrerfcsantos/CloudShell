#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "pedido.h"

#define TAM_MAX_LINHA 1024
#define TAM_MAX_PATH_PROC 1024
#define TAM_MAX_LINHA_PROC 2048

unsigned long ultimaContagemClock=0;
pid_t pid_filho;


unsigned long getClock(pid_t pid){
    int i=1;
    unsigned long resultado;
    char *ptr;
    char *delims = " \n\r\0", *token;
    char *clock_user;
    char *path_proc = (char *) malloc(sizeof(char)*TAM_MAX_PATH_PROC);
    char *linha = (char *) malloc(sizeof(char)*TAM_MAX_LINHA_PROC);
    
    sprintf(path_proc,"/proc/%d/stat", pid);
    FILE *proc_id = fopen(path_proc, "r");
    fgets(linha, TAM_MAX_LINHA_PROC, proc_id);
    
    token = strtok(linha, delims);
    //printf("i = %d | token = %s ", i, token);
    i++;
    
    while(i<=15){
        token = strtok(NULL, delims);
        if(i==14) clock_user = token;
        //printf("i = %d | token = %s ", i, token);
        i++;
    }
    //printf("\n");
    resultado = strtoul(clock_user, &ptr, 10);
    fclose(proc_id);
    return resultado;
}

void terminaFilhos(int sig){
    printf("Termina filhos\n");
    kill(pid_filho, SIGKILL);
}

void contactaContabilizacao(int sig){
    PEDIDO pedido;
    PEDIDO resposta;
    pedido.pid = 1;
    printf("Recebi o alarme. Clock: %lu\n", getClock(getpid()));
    int pid_escrita = open("contas", O_WRONLY);
    int pid_leitura = open("resposta", O_RDONLY);
    
    write(pid_escrita, &pedido , sizeof(PEDIDO));
    read(pid_leitura, &resposta, sizeof(RESPOSTA));
    if(resposta.pid==100) printf("A comunicação correu bem.\n");
    
    close(pid_escrita);
    close(pid_leitura);
    alarm(1);
}

int main(int argc, char** argv) {
    int i=0, estado;
    char *delims = " \r\n", *token;
    pid_t pai = getpid();
    pid_t filho;
    char *linha = (char *) malloc(sizeof(char)*TAM_MAX_LINHA); 
    signal(SIGALRM,contactaContabilizacao);
    signal(SIGINT,terminaFilhos);
    printf("Bash > ");
    
    fgets(linha, TAM_MAX_LINHA, stdin);
    token = strtok(linha, delims);
    
    filho = fork();
    
    if(getpid()!=pai){
        signal(SIGALRM,contactaContabilizacao);
        alarm(1);
        execlp(token, token, NULL);
    } else{
        pid_filho=filho;
        printf("[Bash] pid pai: %d | pid filho: %d\n", getpid(), pid_filho);
        wait(&estado);
        if(WIFEXITED(estado)) {
            printf("O processo terminou normalmente.\n");
        }
        if(WIFSIGNALED(estado)){
            printf("Processo terminado por sinal.\n");
            printf("ID do sinal: %d\n", WTERMSIG(estado));
        }
        printf("[Bash] A sair...\n");
    }
    
    return (EXIT_SUCCESS);
}

