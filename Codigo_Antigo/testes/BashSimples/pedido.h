/* 
 * File:   pedido.h
 * Author: andre
 *
 * Created on May 23, 2015, 5:35 PM
 */

#ifndef PEDIDO_H
#define	PEDIDO_H

struct pedido{
    int pid;
};

typedef struct pedido PEDIDO;

struct resposta{
    int ok_ko;
};

typedef struct resposta RESPOSTA;

#endif	/* PEDIDO_H */

