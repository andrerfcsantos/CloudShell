#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include "pedido.h"


int main(){
    RESPOSTA resposta;
    resposta.ok_ko=0;
    PEDIDO pedido;
    
    int pid_leitura = open("contas", O_RDONLY);
    int pid_escrita = open("resposta", O_WRONLY);
    
    read(pid_leitura, &pedido , sizeof(PEDIDO));
    
    if(pedido.pid == 1) resposta.ok_ko = 100;
    
    write(pid_escrita, &resposta, sizeof(RESPOSTA));
    
    
    
    close(pid_leitura);
    close(pid_escrita);
    return 0;
}
