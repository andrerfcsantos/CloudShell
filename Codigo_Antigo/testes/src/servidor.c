#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "headers/avl.h"
#include "headers/utilizador.h"

#define MAX_LINHA_LEITURA 248

static int comparaUtilizadoresPorUsername(const void *avl_a, const void *avl_b, void *avl_param);
static Utilizador utilizadorFromUsername(char *username);
static void leituraUtilizadores();
static void printArvoreUtilizadores();
static void removeUtilizador(Utilizador novoUtilizador);

static ARVORE utilizadores;

int main() {
    utilizadores = avl_create(comparaUtilizadoresPorUsername, NULL, NULL);
    leituraUtilizadores();
    printArvoreUtilizadores();
}

void registaNovoUtilizador(Utilizador novoUtilizador) {
    Utilizador copia = cloneUtilizador(novoUtilizador);
    avl_insert(utilizadores, copia);
}

static void removeUtilizador(Utilizador novoUtilizador) {
    freeUtilizador((Utilizador) avl_delete(utilizadores, novoUtilizador));
}

Utilizador procuraUtilizador(char *username) {
    Utilizador resultado = avl_find(utilizadores, utilizadorFromUsername(username));
    return resultado;
}

static void printArvoreUtilizadores(){
    Utilizador utilizador_iterado;
    TRAVERSER iterador = avl_t_alloc();
    avl_t_init(iterador, utilizadores);
    printf("Travessia inorder:\n");
    while((utilizador_iterado = avl_t_next(iterador))!=NULL){
        printf("%s\n", toStringUtilizador(utilizador_iterado));
    }
    printf("Travessia concluida.\n");
}

static void leituraUtilizadores() {
    double saldo;
    Utilizador utilizadorficheiro;
    char *username, *password, *token;
    char *delims = " \r\n\0";
    FILE *fich_utilizadores = fopen("../datasets/utilizadores.txt", "r");
    char *linha = (char *) malloc(sizeof (char)*MAX_LINHA_LEITURA);
    
    assert(fich_utilizadores != NULL);
    

    while (fgets(linha, MAX_LINHA_LEITURA, fich_utilizadores)) {
        token = strtok(linha, delims);
        username = token;
        token = strtok(NULL, delims);
        password = token;
        token = strtok(NULL, delims);
        saldo = atof(token);

        utilizadorficheiro = newUtilizadorCompleto(username, password, saldo);
        avl_insert(utilizadores, utilizadorficheiro);
    }

    free(linha);
    fclose(fich_utilizadores);
}

static int comparaUtilizadoresPorUsername(const void *avl_a, const void *avl_b, void *avl_param) {
    Utilizador u1 = (Utilizador) avl_a;
    Utilizador u2 = (Utilizador) avl_b;
    return strcmp(getUsername(u1), getUsername(u2));
}

static Utilizador utilizadorFromUsername(char *username) {
    Utilizador res = (Utilizador) newUtilizador();
    setUsername(res, username);
    return res;
}

