#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>


/*
 * 
 */
int main(int argc, char** argv) {
    int i=0;
    int estado;
    pid_t pid_filho = fork();
    
    if(pid_filho==0){
        while(1) i++;
    }else{
        printf("Sou o processo %d\n", getpid());
        printf("O meu filho tem id: %d\n", pid_filho);
        wait(&estado);
    }
    
    return (EXIT_SUCCESS);
}

