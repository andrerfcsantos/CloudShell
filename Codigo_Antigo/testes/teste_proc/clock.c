#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>

#define TAM_MAX_PATH_PROC 1024
#define TAM_MAX_LINHA_PROC 2048

unsigned long getClock(pid_t pid){
    int i=1;
    unsigned long resultado;
    char *ptr;
    char *delims = " \n\r\0", *token;
    char *tempo_clock;
    char *path_proc = (char *) malloc(sizeof(char)*TAM_MAX_PATH_PROC);
    char *linha = (char *) malloc(sizeof(char)*TAM_MAX_LINHA_PROC);
    
    sprintf(path_proc,"/proc/%d/stat", pid);
    FILE *proc_id = fopen(path_proc, "r");
    fgets(linha, TAM_MAX_LINHA_PROC, proc_id);
    
    token = strtok(linha, delims);
    //printf("i = %d | token = %s ", i, token);
    i++;
    
    while(i<=15){
        token = strtok(NULL, delims);
        //printf("i = %d | token = %s ", i, token);
        if(i==14) tempo_clock = token;
        i++;
    }
    //printf("\n");
    resultado = strtoul(tempo_clock, &ptr, 10);
    fclose(proc_id);
    return resultado;
}

int main(){

	while(1) printf("Clock : %lu\n", getClock(getpid()));

	return 0;
}

