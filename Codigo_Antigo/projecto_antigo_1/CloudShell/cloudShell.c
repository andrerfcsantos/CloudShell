#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "../dados/headers/pedido_autenticacao.h"
#include "../dados/headers/pedido_ok_ko.h"
#include "../dados/headers/resposta_autenticacao.h"
#include "../dados/headers/resposta_ok_ko.h"

#define TAM_MAX_LINHA 1024
#define TAM_MAX_PATH_PROC 1024
#define TAM_MAX_LINHA_PROC 2048

static unsigned long getClock(pid_t pid);
static void terminaFilhos(int sig);
static void contactaServidor(int sig);
static void imprime_como_saiu(int estado);

unsigned long ultima_contagem_clock = 0;
pid_t pid_filho;

int main(int argc, char** argv) {
    int i = 0, estado;
    char *delims = " \r\n", *token;
    char *linha = (char *) malloc(sizeof (char)*TAM_MAX_LINHA);

    pid_t pai = getpid();
    pid_t filho;

    signal(SIGALRM, contactaServidor);
    signal(SIGINT, terminaFilhos);
    printf("CloudShell > ");

    fgets(linha, TAM_MAX_LINHA, stdin);
    token = strtok(linha, delims);

    filho = fork();

    if (getpid() != pai) {
        execlp(token, token, NULL);
        perror("[CloudShell] Falha ao executar exec.\n");
        exit(-1);
    } else {
        pid_filho = filho;
        printf("[CloudShell] A começar processo %d\n", pid_filho);
        alarm(1);
        wait(&estado);
        imprime_como_saiu(estado);
        printf("[CloudShell] A sair...\n");
    }

    return (EXIT_SUCCESS);
}

static void terminaFilhos(int sig) {
    printf("\n");
    printf("[CloudShell] A terminar filho (pid: %d)\n", pid_filho);
    kill(pid_filho, SIGKILL);
}

static void contactaServidor(int sig) {
    PEDIDO_OK_KO pedido;
    RESPOSTA_OK_KO resposta;
    ENUM_RESPOSTA_OK_KO not_ok = NOT_OK;
    unsigned long clock = getClock(pid_filho);
    unsigned long diferenca = clock - ultima_contagem_clock;
    double saldo_a_descontar = (double) diferenca / 100;
    ultima_contagem_clock = clock;
    int pid_escrita = open("../../pipes/pipe_pedido_ok_ko", O_WRONLY);
    int pid_leitura = open("../../pipes/pipe_resposta_ok_ko", O_RDONLY);
    
    printf("--------------------------------------------------------------------\n");
    
    setSaldoADescontar(&pedido, saldo_a_descontar);
    printf("[CloudShell] Recebi o alarme. Clock: %lu Saldo a descontar: %f pids: %d %d\n", 
            clock, getSaldoADescontar(&pedido), pid_escrita, pid_leitura);
    
    if (write(pid_escrita, &pedido, sizeof (PEDIDO_OK_KO)) == sizeof (PEDIDO_OK_KO)) {
        
        printf("[CloudShell] Enviei o pedido com sucesso. {saldo = %f}\n", getSaldoADescontar(&pedido));
        if (read(pid_leitura, &resposta, sizeof (RESPOSTA_OK_KO)) == sizeof (RESPOSTA_OK_KO) ) {
            
            printf("[CloudShell] Li a resposta com sucesso. {resposta= %d}\n", getResposta_ok_ko(&resposta));
            if (getResposta_ok_ko(&resposta) == not_ok) {
                printf("[CloudShell] Recebido NOT_OK. Saldo restante: %f\n", getSaldoRestante(&resposta));
            }else{
                if(getResposta_ok_ko(&resposta) == OK) 
                    printf("CloudShell] Recebido OK. Saldo restante: %f\n", getSaldoRestante(&resposta));
            }
        }
    }
    
    close(pid_escrita);
    close(pid_leitura);

    alarm(1);
}

static unsigned long getClock(pid_t pid) {
    int i = 1;
    unsigned long resultado;
    char *clock_user, *ptr;
    char *delims = " \n\r\0", *token;
    char *path_proc = (char *) malloc(sizeof (char)*TAM_MAX_PATH_PROC);
    char *linha = (char *) malloc(sizeof (char)*TAM_MAX_LINHA_PROC);

    sprintf(path_proc, "/proc/%d/stat", pid);
    FILE *proc_id = fopen(path_proc, "r");
    
    /* TODO: Melhorar tratamento deste erro.*/
    if (proc_id == NULL) {
        printf("[CloudShell] Impossivel encontrar ficheiro em proc.\n");
        return 0;
    }
    
    fgets(linha, TAM_MAX_LINHA_PROC, proc_id);
    token = strtok(linha, delims);
    i++;

    while (i <= 15) {
        token = strtok(NULL, delims);
        if (i == 14) clock_user = token;
        i++;
    }
    
    resultado = strtoul(clock_user, &ptr, 10);

    free(path_proc);
    free(linha);
    fclose(proc_id);
    
    return resultado;
}

static void imprime_como_saiu(int estado) {
    if (WIFEXITED(estado)) {
        printf("[CloudShell] O processo terminou. (estado: %d)\n", WEXITSTATUS(estado));
    }
    if (WIFSIGNALED(estado)) {
        printf("[CloudShell] Processo terminado por sinal (id: %d)\n", WTERMSIG(estado));
    }

}


