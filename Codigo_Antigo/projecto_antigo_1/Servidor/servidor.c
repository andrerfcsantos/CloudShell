#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "../dados/headers/pedido_autenticacao.h"
#include "../dados/headers/pedido_ok_ko.h"
#include "../dados/headers/resposta_autenticacao.h"
#include "../dados/headers/resposta_ok_ko.h"

static void responde_pedido();

double saldo = 10.0;

int main(int argc, char** argv) {
    int estado;
    pid_t pai = getpid();
    pid_t pid_filho;

    pid_filho = fork();

    if (getpid() != pai) {
        while (1) responde_pedido();
    }else{
        wait(&estado);
    }
    
    return (EXIT_SUCCESS);
}

static void responde_pedido() {
    RESPOSTA_OK_KO resposta;
    PEDIDO_OK_KO pedido;
    double saldo_a_descontar;

    int pid_leitura = open("../../pipes/pipe_pedido_ok_ko", O_RDONLY);
    int pid_escrita = open("../../pipes/pipe_resposta_ok_ko", O_WRONLY);

    if (read(pid_leitura, &pedido, sizeof (PEDIDO_OK_KO)) > 0) {
        printf("[Servidor] Li o pedido com sucesso.{saldo a descontar: %f}\n", getSaldoADescontar(&pedido));
        saldo_a_descontar = getSaldoADescontar(&pedido);

        if (saldo_a_descontar <= saldo) {
            saldo -= saldo_a_descontar;
            setResposta_ok_ko(&resposta, OK);
        } else {
            setResposta_ok_ko(&resposta, NOT_OK);
        }

        setSaldoRestante(&resposta, saldo);
        write(pid_escrita, &resposta, sizeof (RESPOSTA_OK_KO));
    }
    
    close(pid_leitura);
    close(pid_escrita);
}
