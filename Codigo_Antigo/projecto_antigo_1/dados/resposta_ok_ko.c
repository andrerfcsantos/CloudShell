#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "headers/resposta_ok_ko.h"

ENUM_RESPOSTA_OK_KO getResposta_ok_ko(AP_RESPOSTA_OK_KO resp_ok_ko) {
    return resp_ok_ko->resposta;
}
double getSaldoRestante(AP_RESPOSTA_OK_KO resp_ok_ko) {
    return resp_ok_ko->saldo_restante;
}

void setSaldoRestante(AP_RESPOSTA_OK_KO resp_ok_ko, double saldo) {
    resp_ok_ko->saldo_restante = saldo;
}

void setResposta_ok_ko(AP_RESPOSTA_OK_KO resp_ok_ko, ENUM_RESPOSTA_OK_KO resp) {
    resp_ok_ko->resposta = resp;
}

