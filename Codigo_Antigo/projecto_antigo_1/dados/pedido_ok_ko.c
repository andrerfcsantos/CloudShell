#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "headers/pedido_ok_ko.h"


double getSaldoADescontar(AP_PEDIDO_OK_KO pedido){
	return pedido->saldo_a_descontar;
}

void setSaldoADescontar(AP_PEDIDO_OK_KO pedido, double saldo){
	pedido->saldo_a_descontar = saldo;
}
