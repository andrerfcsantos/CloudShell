#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "headers/resposta_autenticacao.h"

char *getUsernameRespostaAutenticacao(AP_RESPOSTA_AUTENTICACAO resposta){
	return resposta->username;
}

char *getPasswordRespostaAutenticacao(AP_RESPOSTA_AUTENTICACAO resposta){
	return resposta->password;
}

void setUsernameRespostaAutenticacao(AP_RESPOSTA_AUTENTICACAO resposta, char *username){
	resposta->username = username;
}

void setPasswordRespostaAutenticacao(AP_RESPOSTA_AUTENTICACAO resposta, char *password){
	resposta->password = password;
}


