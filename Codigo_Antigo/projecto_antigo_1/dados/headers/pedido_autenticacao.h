#ifndef PEDIDO_AUTENTICACAO_H
#define	PEDIDO_AUTENTICACAO_H

struct pedido_autenticacao{
	char *username;
	char *password;
};

typedef struct pedido_autenticacao PEDIDO_AUTENTICACAO;
typedef struct pedido_autenticacao *AP_PEDIDO_AUTENTICACAO;

char *getUsernamePedidoAutenticacao(AP_PEDIDO_AUTENTICACAO pedido);
char *getPasswordPedidoAutenticacao(AP_PEDIDO_AUTENTICACAO pedido);
void setUsernamePedidoAutenticacao(AP_PEDIDO_AUTENTICACAO pedido, char *username);
void setPasswordPedidoAutenticacao(AP_PEDIDO_AUTENTICACAO pedido, char *password);


#endif



