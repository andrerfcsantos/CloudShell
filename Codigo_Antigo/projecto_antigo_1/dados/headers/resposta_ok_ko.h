#ifndef RESPOSTA_OK_KO_H
#define	RESPOSTA_OK_KO_H

enum enum_resposta_ok_ko{
	OK=1, NOT_OK=0
};

typedef enum enum_resposta_ok_ko ENUM_RESPOSTA_OK_KO;

struct resposta_ok_ko{
	ENUM_RESPOSTA_OK_KO resposta;
        double saldo_restante;
};

typedef struct resposta_ok_ko RESPOSTA_OK_KO;
typedef struct resposta_ok_ko *AP_RESPOSTA_OK_KO;

ENUM_RESPOSTA_OK_KO getResposta_ok_ko(AP_RESPOSTA_OK_KO resp_ok_ko);
void setResposta_ok_ko(AP_RESPOSTA_OK_KO resp_ok_ko, ENUM_RESPOSTA_OK_KO resp);
void setSaldoRestante(AP_RESPOSTA_OK_KO resp_ok_ko, double saldo);
double getSaldoRestante(AP_RESPOSTA_OK_KO resp_ok_ko);



#endif