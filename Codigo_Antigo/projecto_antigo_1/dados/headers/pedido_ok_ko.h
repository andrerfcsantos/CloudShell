#ifndef PEDIDO_OK_KO_H
#define	PEDIDO_OK_KO_H

struct pedido_ok_ko{
	double saldo_a_descontar;
};

typedef struct pedido_ok_ko PEDIDO_OK_KO;
typedef struct pedido_ok_ko *AP_PEDIDO_OK_KO;

double getSaldoADescontar(AP_PEDIDO_OK_KO pedido);
void setSaldoADescontar(AP_PEDIDO_OK_KO pedido, double saldo);

#endif