#ifndef RESPOSTA_AUTENTICACAO_H
#define	RESPOSTA_AUTENTICACAO_H



struct resposta_autenticacao{
	char *username;
	char *password;
};

typedef struct resposta_autenticacao RESPOSTA_AUTENTICACAO;
typedef struct resposta_autenticacao *AP_RESPOSTA_AUTENTICACAO;

char *getUsernameRespostaAutenticacao(AP_RESPOSTA_AUTENTICACAO resposta);
char *getPasswordRespostaAutenticacao(AP_RESPOSTA_AUTENTICACAO resposta);
void setUsernameRespostaAutenticacao(AP_RESPOSTA_AUTENTICACAO resposta, char *username);
void setPasswordRespostaAutenticacao(AP_RESPOSTA_AUTENTICACAO resposta, char *password);


#endif
