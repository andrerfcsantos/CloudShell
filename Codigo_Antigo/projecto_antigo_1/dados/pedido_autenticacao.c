#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "headers/pedido_autenticacao.h"

char *getUsernamePedidoAutenticacao(AP_PEDIDO_AUTENTICACAO pedido){
	return pedido->username;
}

char *getPasswordPedidoAutenticacao(AP_PEDIDO_AUTENTICACAO pedido){
	return pedido->password;
}

void setUsernamePedidoAutenticacao(AP_PEDIDO_AUTENTICACAO pedido, char *username){
	pedido->username = username;
}

void setPasswordPedidoAutenticacao(AP_PEDIDO_AUTENTICACAO pedido, char *password){
	pedido->password = password;
}


