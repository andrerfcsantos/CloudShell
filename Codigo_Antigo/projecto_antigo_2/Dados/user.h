#ifndef UTILIZADOR_H
#define UTILIZADOR_H

typedef struct utilizador *Utilizador;

Utilizador newUtilizador(void);
Utilizador newUtilizadorCompleto(char *username, char *password, double saldo);
void freeUtilizador(Utilizador utilizador);
void setUsername(Utilizador utilizador, char *novoUsername);
void setPassword(Utilizador utilizador, char *novaPassword);
void setSaldo(Utilizador utilizador, double saldo);
void incSaldo(Utilizador utilizador, double incremento);
char *getUsername(Utilizador utilizador);
char *getPassword(Utilizador utilizador);
double getSaldo(Utilizador utilizador);
char *toStringUtilizador(Utilizador utilizador);
Utilizador cloneUtilizador(Utilizador utilizador);
_Bool equalsUtilizador(Utilizador u1, Utilizador u2);


#endif