#include <stdio.h>
#include <stdlib.h>

struct user{
	char* username;
    char* password;
    double saldo;
}


/*Construtor vazio*/
Utilizador newUtilizador() {
    Utilizador res = (Utilizador) malloc(sizeof (struct utilizador));
    return res;

}

Utilizador newUtilizadorCompleto(char* username, char* password, double saldo) {
    Utilizador res = newUtilizador();
    res->username = (char*) malloc(sizeof (char*) * (strlen(username) + 1));
    strcpy(res->username, username);
    res->password = (char*) malloc(sizeof (char*) * (strlen(password) + 1));
    strcpy(res->password, password);
    res->saldo = saldo;
    return res;

}

/* Free */
void freeUtilizador(Utilizador utilizador) {
    if (free != NULL) {
        free(utilizador->username);
        free(utilizador->password);
    }
    free(utilizador);
}

/*Get's e Set's*/

void setUsername(Utilizador utilizador, char* novoUsername) {
    free(utilizador->username);
    utilizador->username = (char*) malloc(sizeof (char*) * (strlen(novoUsername) + 1));
    strcpy(utilizador->username, novoUsername);

}

void setPassword(Utilizador utilizador, char* novaPassword) {
    free(utilizador->password);
    utilizador->password = (char*) malloc(sizeof (char*) * (strlen(novaPassword) + 1));
    strcpy(utilizador->password, novaPassword);

}

void setSaldo(Utilizador utilizador, double saldo) {
    utilizador->saldo = saldo;
}

void incSaldo(Utilizador utilizador, double incremento) {
    utilizador->saldo += incremento;
}

char* getUsername(Utilizador utilizador) {
    char* username = (char*) malloc(sizeof (char)*(strlen(utilizador->username) + 1));
    strcpy(username, utilizador->username);
    return username;
}

char* getPassword(Utilizador utilizador) {
    char* pass = (char*) malloc(sizeof (char)*(strlen(utilizador->password) + 1));
    strcpy(pass, utilizador->password);
    return pass;
}

double getSaldo(Utilizador utilizador) {
    return utilizador->saldo;
}

int isPasswordCorrect(Utilizador utilizador, char* password){
	if( strcmp(utilizador->password, password ) == 0 ){
		return 1;
	}
	else return 0;

}

int changePassword(Utilizador utilizador, char* passwordantiga, char* passwordnova){
	if(strcmp(utilizador->password, passwordantiga ) == 0 ){
		//mesmas passwords...
		setPassword(utilizador, passwordnova);
		return 1;
	}
	return 0;
	//Passwords antiga e a que tenho nao sao iguais. Nao pode mudar.
}

/* toString, clone, equals */

char* toStringUtilizador(Utilizador utilizador) {
    char* retorno = (char*) malloc(sizeof (char)* MAX_STRING);
    sprintf(retorno, "Nome: %s | Password: %s | Saldo: %f",
            utilizador->username, utilizador->password, utilizador->saldo);
    return retorno;
}

Utilizador cloneUtilizador(Utilizador utilizador) {
    Utilizador novo = newUtilizadorCompleto(utilizador->username, utilizador->password, utilizador->saldo);
    return novo;
}

bool equalsUtilizador(Utilizador u1, Utilizador u2) {
    return (strcmp(u1->username, u2->username) == 0) &&
            (strcmp(u1->password, u2->password) == 0) &&
            u1->saldo == u2->saldo;

}
