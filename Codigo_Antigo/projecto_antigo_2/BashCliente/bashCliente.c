#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "../EstruturasPipes/pedido_execucao.h"


#define MAX_LINHA 1024
#define MAX_PATH_PIPE 1024
#define MAX_BUFFER_LEITURA_OUTPUT 1024

static void cria_pipes();
static void bash();
static void autenticacao();
static void gere_pedido(char *linha_a_enviar);

int main(int argc, char** argv) {
    cria_pipes();
    bash();
    return (EXIT_SUCCESS);
}

static void cria_pipes(){
    int retorno_mkfifo;
    char path_pipe_resposta_cloud_shell[MAX_PATH_PIPE];
    char *path_pipe_pedido_autenticacao = "../Pipes/pedido_autenticacao";
    char *path_pipe_resposta_autenticacao = "../Pipes/resposta_autenticacao";
    char *path_pipe_comandos = "../Pipes/comandos";
    
    retorno_mkfifo = mkfifo(path_pipe_pedido_autenticacao, 0666);
    if(retorno_mkfifo == -1) printf("Pipe \"%s\" não criado. %s\n", path_pipe_pedido_autenticacao, strerror(errno));
    
    retorno_mkfifo = mkfifo(path_pipe_resposta_autenticacao, 0666);
    if(retorno_mkfifo == -1) printf("Pipe \"%s\" não criado. %s\n", path_pipe_resposta_autenticacao, strerror(errno));
    
    retorno_mkfifo = mkfifo(path_pipe_comandos, 0666);
    if(retorno_mkfifo == -1) printf("Pipe \"%s\" não criado. %s\n", path_pipe_comandos, strerror(errno));
    
    sprintf(path_pipe_resposta_cloud_shell, "../Pipes/%d", getpid());
    retorno_mkfifo = mkfifo(path_pipe_resposta_cloud_shell, 0666);
    if(retorno_mkfifo == -1) printf("Pipe \"%s\" não criado. %s\n", path_pipe_resposta_cloud_shell, strerror(errno));
    
}

static void autenticacao() {
    char *username, *password;
    char *resultado_fgets, *token, *delimitadores = "\n\r";
    char *linha_username = (char *) malloc(sizeof (char)*MAX_LINHA);
    char *linha_password = (char *) malloc(sizeof (char)*MAX_LINHA);

    printf("Bem-vindo à CloudShell.\n");
    printf("Faça a autenticação antes de proseguir.\n");
    printf("Username: ");
    
    if (fgets(linha_username, MAX_LINHA, stdin) != NULL) {
        token = strtok(linha_username, delimitadores);
        username=token;
        printf("Password: ");
        
        if (fgets(linha_password, MAX_LINHA, stdin) != NULL) {
            token = strtok(linha_password, delimitadores);
            password = token;
        }
    }
    
}

static void bash() {
    bool sair = false;
    char *token, *delimitadores = "\n\r\0";
    char *linha = (char *) malloc(sizeof (char)*MAX_LINHA);
    pid_t pid_bash = getpid();


    while (!sair) {
        printf("BashCliente > ");

        if (fgets(linha, MAX_LINHA, stdin) != NULL) {
            token = strtok(linha, delimitadores);
            printf("Linha lida: %s\n", token);
        } else {
            printf("[BashCliente] Não foi possivel ler a linha com sucesso.\n");
        }

        if (strcmp(token, "sair") == 0) {
            sair = true;
        } else {
            gere_pedido(token);
        }

    }
}

static void gere_pedido(char *linha_a_enviar){
    AP_PEDIDO_EXECUCAO pedido = (AP_PEDIDO_EXECUCAO) malloc(sizeof(PEDIDO_EXECUCAO));
    ssize_t retorno_write;
    int fd_escrita, fd_leitura;
    ssize_t bytes_lidos;
    char buffer_leitura[MAX_BUFFER_LEITURA_OUTPUT];
    char path_pipe_resposta_cloud_shell[MAX_PATH_PIPE];
    char *copia_linha = (char*) malloc(sizeof(char)*(strlen(linha_a_enviar)+1));
    strcpy(copia_linha, linha_a_enviar);
    
    setComandoPedidoExecucao(pedido, copia_linha);
    setPidProcessoPedidoExecucao(pedido, getpid());
    
    fd_escrita = open("../Pipes/comandos", O_WRONLY);
    if(fd_escrita == -1) printf("[BashCliente] Erro ao abrir pipe de comandos para escrita. %s\n", strerror(errno));
    
    retorno_write = write(fd_escrita, pedido, sizeof(PEDIDO_EXECUCAO));
    if(retorno_write == -1 ) printf("[BashCliente] Erro ao escrever para pipe de comandos. %s\n", strerror(errno));
    else printf("Pedido enviado {pid: %d, comando: %s}\n", getPidProcessoPedidoExecucao(pedido),
                                                           getComandoPedidoExecucao(pedido));
    
    sprintf(path_pipe_resposta_cloud_shell, "../Pipes/%d", getpid());
    fd_leitura = open(path_pipe_resposta_cloud_shell, O_RDONLY);
    
    while((bytes_lidos = read(fd_leitura, buffer_leitura, MAX_BUFFER_LEITURA_OUTPUT))>0){
        write(1, buffer_leitura, bytes_lidos);
    }
    
}

