#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "../EstruturasPipes/pedido_contabilidade.h"
#include "../EstruturasPipes/resposta_contabilidade.h"

#define TAM_MAX_PATH_PROC 1024
#define TAM_MAX_LINHA_PROC 2048

static void cria_pipes_conta();
static void responde_pedidos_conta();
static unsigned long getClock(pid_t pid);

unsigned long ultima_contagem_clock=0;


double saldo = 10.0;

int main(int argc, char** argv) {
    cria_pipes_conta();
    while(1)responde_pedidos_conta();
    return (EXIT_SUCCESS);
}

static void cria_pipes_conta(){
    int retorno_mkfifo;
    char *path_pipe_pedido_contabilizacao = "../Pipes/pedido_contabilizacao";
    char *path_pipe_resposta_contabilizacao = "../Pipes/resposta_contabilizacao";
    
    retorno_mkfifo = mkfifo(path_pipe_pedido_contabilizacao, 0666);
    if(retorno_mkfifo == -1) printf("Pipe \"%s\" não criado. %s\n", path_pipe_pedido_contabilizacao, strerror(errno));
    
    retorno_mkfifo = mkfifo(path_pipe_resposta_contabilizacao, 0666);
    if(retorno_mkfifo == -1) printf("Pipe \"%s\" não criado. %s\n", path_pipe_resposta_contabilizacao, strerror(errno));
    
}

static void responde_pedidos_conta() {
    RESPOSTA_CONTABILIDADE resposta;
    PEDIDO_CONTABILIDADE pedido;
    ENUM_TIPO_ACCAO accao_pedido;
    unsigned long clock, diferenca;
    double saldo_a_descontar;


    int pid_leitura = open("../Pipes/pedido_contabilizacao", O_RDONLY);
    int pid_escrita = open("../Pipes/resposta_contabilizacao", O_WRONLY);

    if (read(pid_leitura, &pedido, sizeof (PEDIDO_CONTABILIDADE)) > 0) {
        printf("[ServidorConta] Li o pedido com sucesso. "
                "{pid_cliente = %d pid exec = %d accao= %d}\n",
                getPidClientePedidoContabilidade(&pedido), 
                getPidExecucaoPedidoContabilidade(&pedido), 
                getTipoAccaoPedidoContabilidade(&pedido));

        clock = getClock(getPidExecucaoPedidoContabilidade(&pedido));
        diferenca = clock - ultima_contagem_clock;
        saldo_a_descontar = (double) diferenca / 100;
        ultima_contagem_clock = clock;
        accao_pedido = getTipoAccaoPedidoContabilidade(&pedido);


        switch (accao_pedido) {
            case DESCONTAR_SALDO:
                if (saldo >= saldo_a_descontar) {
                    saldo -= saldo_a_descontar;
                    setTipoAccaoRespostaContabilidade(&resposta, OK);
                } else {
                    setTipoAccaoRespostaContabilidade(&resposta, NOT_OK);
                }
                setSaldoActualRespostaContabilidade(&resposta, saldo);
                setSaldoDescontadoRespostaContabilidade(&resposta, saldo_a_descontar);
                setPidClienteRespostaContabilidade(&resposta, getPidClientePedidoContabilidade(&pedido));
                setPidExecRespostaContabilidade(&resposta, getPidExecucaoPedidoContabilidade(&pedido));
                break;
            case VERIFICAR_SALDO:
                break;
            case ADICIONAR_PID:
                break;
            case REMOVER_PID:
                break;
            default:
                break;


        }

        if (write(pid_escrita, &resposta, sizeof (RESPOSTA_CONTABILIDADE)) < 0) {
            printf("[ServidorConta] Erro na escrita da resposta.%s\n", strerror(errno));
        } else {
            printf("[ServidorConta] Enviei a resposta com sucesso. "
                    "{pid_cliente = %d, pid_exec = %d, saldo actual = %f, saldo descontado = %f,  resposta= %d}\n",
                    getPidClienteRespostaContabilidade(&resposta),
                    getPidExecRespostaContabilidade(&resposta), 
                    getSaldoActualRespostaContabilidade(&resposta),
                    getSaldoDescontadoRespostaContabilidade(&resposta),
                    getTipoAccaoRespostaContabilidade(&resposta));
        }
    } else {
        printf("[ServidorConta] Erro na leitura de pedido.%s\n", strerror(errno));
    }

    close(pid_leitura);
    close(pid_escrita);
}

static unsigned long getClock(pid_t pid) {
    int i = 1;
    unsigned long resultado;
    char *clock_user, *ptr;
    char *delims = " \n\r\0", *token;
    char *path_proc = (char *) malloc(sizeof (char)*TAM_MAX_PATH_PROC);
    char *linha = (char *) malloc(sizeof (char)*TAM_MAX_LINHA_PROC);

    sprintf(path_proc, "/proc/%d/stat", pid);
    FILE *proc_id = fopen(path_proc, "r");
    
    /* TODO: Melhorar tratamento deste erro.*/
    if (proc_id == NULL) {
        printf("[CloudShell] Impossivel encontrar ficheiro em proc.\n");
        return 0;
    }
    
    fgets(linha, TAM_MAX_LINHA_PROC, proc_id);
    token = strtok(linha, delims);
    i++;

    while (i <= 15) {
        token = strtok(NULL, delims);
        if (i == 14) clock_user = token;
        i++;
    }
    
    resultado = strtoul(clock_user, &ptr, 10);

    free(path_proc);
    free(linha);
    fclose(proc_id);
    
    return resultado;
}

