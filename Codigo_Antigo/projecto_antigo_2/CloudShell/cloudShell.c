#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include "../EstruturasPipes/pedido_execucao.h"
#include "../EstruturasPipes/pedido_contabilidade.h"
#include "../EstruturasPipes/resposta_contabilidade.h"

#define P_LEITURA 0
#define P_ESCRITA 1
#define MAX_PATH_PIPE_ 1024
#define MAX_ARGS 128

static void envia_pedido();
static char **argsToArray(char *linha);
static char **cmdsLinhaToArray(char *linha);
static int length_array(char ** array);


pid_t pid_filho_global;

int main(int argc, char** argv) {
    AP_PEDIDO_EXECUCAO pedido = (AP_PEDIDO_EXECUCAO) malloc(sizeof (PEDIDO_EXECUCAO));
    int estado;
    char path_pipe_resposta_cloud_shell[MAX_PATH_PIPE_];
    char *comando;
    char **comando_args;
    char **comandos_pipes;
    pid_t pid_pedido;
    pid_t pid_filho;
    ssize_t retorno_read;
    int fd_leitura;
    int fd_escrita;
    pid_t pid_pai = getpid();

    signal(SIGALRM, envia_pedido);

    fd_leitura = open("../Pipes/comandos", O_RDONLY);
    if (fd_leitura == -1) printf("[CloudShell] Erro ao abrir pipe para leitura de comando. %s\n", strerror(errno));

    while (1) {
        retorno_read = read(fd_leitura, pedido, sizeof (PEDIDO_EXECUCAO));

        if (retorno_read < 0) {
            printf("[CloudShell] Erro ao ler de pipe de comandos. %s\n", strerror(errno));
        } else {
            printf("[CloudShell] Pedido recebido { pid_pedido =  %d, comando = %s}\n",
                    getPidProcessoPedidoExecucao(pedido), getComandoPedidoExecucao(pedido));

            pid_pedido = getPidProcessoPedidoExecucao(pedido);
            comando = getComandoPedidoExecucao(pedido);


            pid_filho = fork();

            if (getpid() == pid_pai) {
                pid_filho_global = pid_filho;
                alarm(1);
                wait(&estado);
            } else {
                comando_args = argsToArray(comando);
                sprintf(path_pipe_resposta_cloud_shell, "../Pipes/%d", pid_pedido);
                fd_escrita = open(path_pipe_resposta_cloud_shell, O_WRONLY);
                dup2(fd_escrita, 1);
                execvp(comando_args[0], comando_args);
                printf("[CloudShell] Erro no exec. %s\n", strerror(errno));
                _exit(-1);
            }

        }
    }

    return (EXIT_SUCCESS);
}

static int length_array(char ** array) {
    int i = 0, soma = 0;
    while (array[i] != NULL) soma++;
    return soma;
}

static char **argsToArray(char *linha) {
    int i = 0;
    char *token, *delimitadores = " \n\r";
    char **array = (char **) malloc(sizeof (char *)*MAX_ARGS);
    char *linha_copia = (char *) malloc(sizeof (char)*(strlen(linha) + 1));
    strcpy(linha_copia, linha);

    token = strtok(linha_copia, delimitadores);
    array[i++] = token;

    while ((token = strtok(NULL, delimitadores)) != NULL && i < MAX_ARGS - 1) {
        array[i++] = token;
    }

    array[i] = NULL;
    return array;
}

static char **cmdsLinhaToArray(char *linha) {
    int i = 0;
    char *token, *delimitadores = "|\n\r";
    char **array = (char **) malloc(sizeof (char *)*MAX_ARGS);
    char *linha_copia = (char *) malloc(sizeof (char)*(strlen(linha) + 1));
    strcpy(linha_copia, linha);

    token = strtok(linha_copia, delimitadores);
    array[i++] = token;

    while ((token = strtok(NULL, delimitadores)) != NULL && i < MAX_ARGS - 1) {
        array[i++] = token;
    }

    array[i] = NULL;
    return array;
}

static void envia_pedido(int sig) {
    PEDIDO_CONTABILIDADE pedido;
    RESPOSTA_CONTABILIDADE resposta;
    ENUM_IS_OK not_ok = NOT_OK;


    int pid_escrita = open("../Pipes/pedido_contabilizacao", O_WRONLY);
    int pid_leitura = open("../Pipes/resposta_contabilizacao", O_RDONLY);

    printf("--------------------------------------------------------------------\n");

    setPidClientePedidoContabilidade(&pedido, getpid());
    setPidExecucaoPedidoContabilidade(&pedido, pid_filho_global);
    setTipoAccaoContabilidade(&pedido, DESCONTAR_SALDO);

    printf("[CloudShell] Recebi o alarme e abri pipes %d %d\n", pid_escrita, pid_leitura);

    if (write(pid_escrita, &pedido, sizeof (PEDIDO_CONTABILIDADE)) == sizeof (PEDIDO_CONTABILIDADE)) {

        printf("[CloudShell] Enviei o pedido com sucesso. {pid_cliente = %d  pid_exec = %d accao= %d}\n",
                getPidClientePedidoContabilidade(&pedido), getPidExecucaoPedidoContabilidade(&pedido), getTipoAccaoPedidoContabilidade(&pedido));

        if (read(pid_leitura, &resposta, sizeof (RESPOSTA_CONTABILIDADE)) == sizeof (RESPOSTA_CONTABILIDADE)) {

            printf("[CloudShell] Li a resposta com sucesso. "
                    "{pid_cliente = %d, pid_exec = %d, resposta= %d, saldo restante = %f, saldo descontado = %f}\n",
                    getPidClienteRespostaContabilidade(&resposta),
                    getPidExecRespostaContabilidade(&resposta),
                    getTipoAccaoRespostaContabilidade(&resposta),
                    getSaldoActualRespostaContabilidade(&resposta),
                    getSaldoDescontadoRespostaContabilidade(&resposta));

            if (getTipoAccaoRespostaContabilidade(&resposta) == NOT_OK) {
                kill(pid_filho_global, SIGKILL);
            }
        }
    }

    close(pid_escrita);
    close(pid_leitura);

    alarm(1);
}
