#ifndef RESPOSTA_CONTABILIDADE_H
#define RESPOSTA_CONTABILIDADE_H

enum enum_is_ok{
    OK=1, NOT_OK
};

typedef enum enum_is_ok ENUM_IS_OK;

struct resposta_contabilidade{
    int pid_cliente_resposta;
    int pid_exec_resposta;
    double saldo_restante;
    double saldo_descontado;
    enum enum_is_ok is_ok;
};

typedef struct resposta_contabilidade RESPOSTA_CONTABILIDADE;
typedef struct resposta_contabilidade *AP_RESPOSTA_CONTABILIDADE;

pid_t getPidClienteRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta);
pid_t getPidExecRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta);
double getSaldoActualRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta);
double getSaldoDescontadoRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta);
ENUM_IS_OK getTipoAccaoRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta);

void setPidClienteRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta, int pid);
void setPidExecRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta, int pid);
void setSaldoActualRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta, double saldo);
void setSaldoDescontadoRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta, double saldo);
void setTipoAccaoRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta, ENUM_IS_OK is_ok);


#endif