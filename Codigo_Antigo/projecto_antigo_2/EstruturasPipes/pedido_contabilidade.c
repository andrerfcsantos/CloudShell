#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "pedido_contabilidade.h"

pid_t getPidClientePedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido){
    return pedido->pid_cliente;
}

pid_t getPidExecucaoPedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido){
    return pedido->pid_execucao;
}

double getSaldoPedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido){
    return pedido->saldo;
}

ENUM_TIPO_ACCAO getTipoAccaoPedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido){
    return pedido->accao;
}

void setPidClientePedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido, int pid){
    pedido->pid_cliente = pid;
}
void setPidExecucaoPedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido, int pid){
    pedido->pid_execucao = pid;
}

void setSaldoPedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido, double saldo){
    pedido->saldo = saldo;
}

void setTipoAccaoContabilidade(AP_PEDIDO_CONTABILIDADE pedido, ENUM_TIPO_ACCAO accao){
    pedido->accao = accao;
}

