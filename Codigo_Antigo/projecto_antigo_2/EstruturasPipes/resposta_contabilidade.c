#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "resposta_contabilidade.h"


pid_t getPidClienteRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta){
    return resposta->pid_cliente_resposta;
}

pid_t getPidExecRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta){
    return resposta->pid_exec_resposta;
}

double getSaldoActualRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta){
    return resposta->saldo_restante;
}

double getSaldoDescontadoRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta){
    return resposta->saldo_descontado;
}

ENUM_IS_OK getTipoAccaoRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta){
    return resposta->is_ok;
}



void setPidClienteRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta, int pid){
    resposta->pid_cliente_resposta=pid;
}

void setPidExecRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta, int pid){
    resposta->pid_exec_resposta=pid;
}

void setSaldoActualRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta, double saldo){
    resposta->saldo_restante=saldo;
}

void setSaldoDescontadoRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta, double saldo){
    resposta->saldo_descontado=saldo;
}



void setTipoAccaoRespostaContabilidade(AP_RESPOSTA_CONTABILIDADE resposta, ENUM_IS_OK is_ok){
    resposta->is_ok = is_ok;
}

