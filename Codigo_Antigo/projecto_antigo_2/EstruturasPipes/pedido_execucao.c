#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pedido_execucao.h"

int getPidProcessoPedidoExecucao(AP_PEDIDO_EXECUCAO pedido){
    return pedido->pid_processo_cliente;
}

char *getUsernamePedidoExecucao(AP_PEDIDO_EXECUCAO pedido){
    return pedido->username;
}

char *getComandoPedidoExecucao(AP_PEDIDO_EXECUCAO pedido){
    return pedido->comando;
}

void setPidProcessoPedidoExecucao(AP_PEDIDO_EXECUCAO pedido, int pid){
   pedido->pid_processo_cliente=pid;
}

void *setUsernamePedidoExecucao(AP_PEDIDO_EXECUCAO pedido, char* username){
    pedido->username = username;
}

void *setComandoPedidoExecucao(AP_PEDIDO_EXECUCAO pedido, char * comando){
     strcpy(pedido->comando,comando);
}
