#ifndef PEDIDO_CONTABILIDADE_H
#define PEDIDO_CONTABILIDADE_H

enum enum_tipo_accao{
    DESCONTAR_SALDO=1, VERIFICAR_SALDO, ADICIONAR_PID, REMOVER_PID
};

typedef enum enum_tipo_accao ENUM_TIPO_ACCAO;

struct pedido_contabilidade{
    int pid_cliente;
    int pid_execucao;
    double saldo;
    enum enum_tipo_accao accao;
};

typedef struct pedido_contabilidade PEDIDO_CONTABILIDADE;
typedef struct pedido_contabilidade *AP_PEDIDO_CONTABILIDADE;


pid_t getPidClientePedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido);
pid_t getPidExecucaoPedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido);
double getSaldoPedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido);
ENUM_TIPO_ACCAO getTipoAccaoPedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido);

void setPidClientePedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido, int pid);
void setPidExecucaoPedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido, int pid);
void setSaldoPedidoContabilidade(AP_PEDIDO_CONTABILIDADE pedido, double saldo);
void setTipoAccaoContabilidade(AP_PEDIDO_CONTABILIDADE pedido, ENUM_TIPO_ACCAO accao);

#endif