#ifndef PEDIDO_EXECUCAO_H
#define PEDIDO_EXECUCAO_H

struct pedido_execucao{
    int pid_processo_cliente;
    char *username;
    char comando[2048];
};

typedef struct pedido_execucao PEDIDO_EXECUCAO;
typedef struct pedido_execucao *AP_PEDIDO_EXECUCAO;

int getPidProcessoPedidoExecucao(AP_PEDIDO_EXECUCAO pedido);
char *getUsernamePedidoExecucao(AP_PEDIDO_EXECUCAO pedido);
char *getComandoPedidoExecucao(AP_PEDIDO_EXECUCAO pedido);
void setPidProcessoPedidoExecucao(AP_PEDIDO_EXECUCAO pedido, int pid);
void *setUsernamePedidoExecucao(AP_PEDIDO_EXECUCAO pedido, char* username);
void *setComandoPedidoExecucao(AP_PEDIDO_EXECUCAO pedido, char * comando);

#endif