#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define P_LEITURA 0
#define P_ESCRITA 1
#define TAM_MSG 100
#define TAM_LINHA 100
#define PIPE_EOF 0
#define MAX_ARGS 128
/*
 - Fecho de descritores nos filhos
 - Copia de apontadores para filhos ou nova alocaçao
 - O write escreve de uma so vez?
 - Porque é que o read nao le de uma so vez?
 */

void ex1();
void ex2();
void ex3();
void ex4();
void ex5();
void exo5();
void geral();
void geralcerta();

int main(int argc, char** argv) {
    geralcerta();
    return (EXIT_SUCCESS);
}

void ex1() {
    int pipe1[2];
    pid_t pai = getpid();
    pid_t filho;
    char *buff_escrita = (char *) malloc(sizeof (char)*TAM_MSG);
    char *buff_leitura = (char *) malloc(sizeof (char)*TAM_MSG);

    pipe(pipe1);
    filho = fork();

    if (getpid() != pai) {

        close(pipe1[P_ESCRITA]);

        while ((read(pipe1[P_LEITURA], buff_leitura, strlen(buff_leitura) + 1)) != PIPE_EOF)
            printf("%s", buff_leitura);

        printf("\n");

    } else {
        buff_escrita = "ola";
        close(pipe1[P_LEITURA]);
        write(pipe1[P_ESCRITA], buff_escrita, strlen(buff_escrita) + 1);
        close(pipe1[1]);
    }

}

void ex2() {
    ex1();
}

void ex3() {
    int pipe1[2];
    pid_t pai = getpid();
    pid_t filho;
    int tamanho;
    char *linha = NULL;

    pipe(pipe1);
    filho = fork();

    if (getpid() != pai) {
        close(pipe1[P_ESCRITA]);
        dup2(pipe1[P_LEITURA], 0);
        system("wc");
    } else {
        close(pipe1[P_LEITURA]);
        linha = (char *) malloc(sizeof (char)*(TAM_LINHA));

        while (fgets(linha, TAM_LINHA, stdin) != NULL) {
            tamanho = strlen(linha);
            write(pipe1[P_ESCRITA], linha, tamanho + 1);
        }

    }

}

void ex4() {
    int pipe1[2];
    pid_t pai = getpid();
    pid_t filho;
    pipe(pipe1);
    filho = fork();

    if (getpid() == pai) {
        close(pipe1[P_ESCRITA]);
        dup2(pipe1[P_LEITURA], 0);
        system("wc -l");
    } else {
        close(pipe1[P_LEITURA]);
        dup2(pipe1[P_ESCRITA], 1);
        system("ls /etc");
    }

}

void ex5() {
    int pipe1[2];
    int pipe2[2];
    int pipe3[2];
    pid_t pai = getpid();
    pid_t filho;
    pid_t filhos[4];
    int i;
    pipe(pipe1);
    pipe(pipe2);
    pipe(pipe3);

    for (i = 0; i < 4; i++) {
        if (getpid() == pai) {
            filho = fork();
            filhos[i] = filho;
        }
    }

    if (getpid() == filhos[0]) {
        close(pipe1[P_LEITURA]);
        close(pipe2[P_LEITURA]);
        close(pipe2[P_ESCRITA]);
        close(pipe3[P_LEITURA]);
        close(pipe3[P_ESCRITA]);

        dup2(pipe1[P_ESCRITA], 1);
        system("grep -v ˆ# /etc/passwd");
    }
    
    
    
    if (getpid() == filhos[1]) {
        close(pipe1[P_ESCRITA]);
        close(pipe2[P_LEITURA]);
        close(pipe3[P_LEITURA]);
        close(pipe3[P_ESCRITA]);

        dup2(pipe2[P_ESCRITA], 1);
        dup2(pipe1[P_LEITURA], 0);
        system("cut -f7 -d:");
    }
    
    if (getpid() == filhos[2]) {
        close(pipe1[P_LEITURA]);
        close(pipe1[P_ESCRITA]);
        close(pipe2[P_ESCRITA]);
        close(pipe3[P_LEITURA]);

        dup2(pipe3[P_ESCRITA], 1);
        dup2(pipe2[P_LEITURA], 0);
        system("uniq");
    }
    
    if (getpid() == filhos[3]) {
        close(pipe1[P_LEITURA]);
        close(pipe1[P_ESCRITA]);
        close(pipe2[P_LEITURA]);
        close(pipe2[P_ESCRITA]);
        close(pipe3[P_ESCRITA]);

        system("wc -l");
    }

}


void exo5(){

    int pipe1[2], pipe2[2], pipe3[2];
pipe(pipe1);
    if(fork()==0){
        close(pipe1[0]); dup2(pipe1[1],1); close(pipe1[1]); system("grep -v ^# /etc/passwd"); 
    }
    else{   
        close(pipe1[1]);
        pipe(pipe2);
        if(fork()==0){ 
            dup2(pipe1[0], 0); //Antes de fechar a leitura passar po pipe seguinte o que leu da escrita anterior...
            close(pipe1[0]); //ou seja, guardei o grep...
            dup2(pipe2[1], 1);
            close(pipe2[1]);
            close(pipe2[0]);
            system("cut -f7 -d:");

         }
         else{
            
            close(pipe1[0]);
            close(pipe2[1]);
          
            pipe(pipe3);
            if(fork()==0){
                 dup2(pipe2[0], 0); close(pipe2[0]); dup2(pipe3[1], 1);
                 close(pipe3[1]);  close(pipe3[0]);
               system("uniq"); 

            }

            else{
                
                close(pipe2[0]);
                close(pipe3[1]); 
                dup2(pipe3[0],0);
                close(pipe3[0]);
                system("wc -l");

            }
           
         }



    }

}

void geral(){
    // char buff[30]; char string[50];
    // char* comandoaux; int tamaux=0; int h=0;
    // printf("Insira quantos comandos quer\n ");
    // scanf("%d", &h);
    
    // char *buffer;
    //printf("Insira os comandos\n");
    //scanf("%s", string); /* String them os comandos todos separados por | */
    //read(0,buffer,100);

    //comandos = (char**) malloc(sizeof(char*)*h);
    //Aloca espaço para array de strings

    // char *comandos[h];
    // comandoaux = strtok(string, "|");
    //   //comandos[i] = strdup(comandoaux);
    
    // int i= 0;
    // while( comandoaux != NULL ){
    //     comandos[i] = strdup(comandoaux);
    //     printf("%s\n", strdup(comandoaux));
    //     comandoaux = strtok(NULL, "|");
    //     i++;
    // }
   
    // int u=0;
    // while(u < h ){
    //     printf("%s", comandos[u]);
    //     u++;
    // }

    char s[1];
    char str[50];

    while(read(0,&s,1)) {
      printf("%s\n", s);
    }


}
static char **cmdsLinhaToArray(char *linha);

void geralcerta(){
    char* linha = "grep -v ˆ# /etc/passwd | cut -f7 -d: | uniq | wc -l";
    //scanf("%s", linha);
    //scanf("%[^\t\n]",linha);

    char** retorno = cmdsLinhaToArray(linha);
    int i=0;

    while(retorno[i] != NULL){
        printf("%s\n", retorno[i]);
        i++;
    }


}

static char **cmdsLinhaToArray(char *linha) {
    int i = 0;
    char *token, *delimitadores = "|\n\r";
    char **array = (char **) malloc(sizeof (char *)*MAX_ARGS);
    char *linha_copia = (char *) malloc(sizeof (char)*(strlen(linha) + 1));
    strcpy(linha_copia, linha);

    token = strtok(linha_copia, delimitadores);
    array[i++] = token;

    while ((token = strtok(NULL, delimitadores)) != NULL && i < MAX_ARGS - 1) {
        array[i++] = token;
    }

    array[i] = NULL;
    return array;
}



