#ifndef PEDIDO_SERVIDOR_H
#define PEDIDO_SERVIDOR_H
#include "../headers/tamanhos_maximos.h"

enum enum_tipo_pedido{
    ADD_USER, ADD_SALDO, EXECUTE, VERIFICA_SALDO
};

typedef enum enum_tipo_pedido TIPO_PEDIDO;

struct pedido_servidor{
    int pid_cliente;
    int pid_exec;
    double saldo;
    enum enum_tipo_pedido tipo_pedido;
};

typedef struct pedido_servidor PEDIDO_SERVIDOR;
typedef struct pedido_servidor *AP_PEDIDO_SERVIDOR;


#endif
