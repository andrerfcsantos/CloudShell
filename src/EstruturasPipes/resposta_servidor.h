#ifndef RESPOSTA_SERVIDOR_H
#define RESPOSTA_SERVIDOR_H

enum enum_tipo_resposta{
    OK=1, NOT_OK=0
};

typedef enum enum_tipo_resposta TIPO_RESPOSTA;


struct resposta_servidor{
    double saldo_restante;
    enum enum_tipo_resposta tipo_resposta;
};

typedef struct resposta_servidor RESPOSTA_SERVIDOR;
typedef struct resposta_servidor *AP_RESPOSTA_SERVIDOR;

#endif
