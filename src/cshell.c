#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdbool.h>
#include <signal.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include "headers/tamanhos_maximos.h"
#include "EstruturasPipes/pedido_servidor.h"
#include "EstruturasPipes/resposta_servidor.h"


static void verifica_saldo();
static AP_RESPOSTA_SERVIDOR adiciona_saldo(double saldo_a_adicionar);
static void adiciona_user();
static void cria_pipes_shell();
static void shell();
static void executa_comando(char *linha);
static void auxiliar_executa_1_comando(char *linha);
static void auxiliar_executa_pipes(char **comandos);
static void envia_pedido_servidor(int sig);
static void fecha_todos_pipes(int **fd_pipes, int num_comandos);
static int**criaMatriz(int linhas, int colunas);
static int length_array(char ** array);
static char **argsToArray(char *linha);
static char **cmdsLinhaToArray(char *linha);

pid_t pid_shell;
pid_t pid_filho_shell;
char path_pipe_resposta_shell[MAX_PATH_PIPE];
char path_pipe_resposta_addsaldo[MAX_PATH_PIPE];
char path_pipe_resposta_verifica_saldo[MAX_PATH_PIPE];
char path_pipe_resposta_adduser[MAX_PATH_PIPE];

int main(int argc, char** argv) {
    pid_shell = getpid();
    cria_pipes_shell();
    adiciona_user();
    signal(SIGALRM, envia_pedido_servidor);
    shell();
    return (EXIT_SUCCESS);
}

void shell() {
    bool sair = false, leu_comando = false;
    double saldo;
    char *token, *delimitadores = "\n\r\0";
    char linha[MAX_LINHA_BASH];

    printf("[BashCliente] Bem vindo à CloudShell.\n");
    printf("[BashCliente] O seu identificador de sessão: %d\n", pid_shell);


    while (!sair) {
        leu_comando = false;
        printf("BashCliente > ");

        if (fgets(linha, MAX_LINHA_BASH, stdin) != NULL) {
            token = strtok(linha, delimitadores);
        } else {
            printf("[BashCliente] Não foi possivel ler a linha com sucesso.\n");
        }

        if (token != NULL && strcmp(token, "sair") == 0) {
            leu_comando = true;
            sair = true;
        }

        if (token != NULL && strstr(token, "addsaldo") != NULL && !leu_comando) {
            leu_comando = true;
            char **argumentos = argsToArray(linha);
            if (length_array(argumentos) == 2) {
                saldo = atof(argumentos[1]);
                adiciona_saldo(saldo);
            }
        }
        if (token != NULL && strstr(token, "removesaldo") != NULL && !leu_comando) {
            leu_comando = true;
            char **argumentos = argsToArray(linha);
            if (length_array(argumentos) == 2) {
                saldo = atof(argumentos[1]);
                adiciona_saldo(0.0 - saldo);
            }
        }

        if (token != NULL && strstr(token, "verificasaldo") != NULL && !leu_comando) {
            leu_comando = true;
            verifica_saldo(token);
        }

        if (token != NULL && !sair && !leu_comando) {
            AP_RESPOSTA_SERVIDOR resposta = adiciona_saldo(0 - 0.5);
            if (resposta->tipo_resposta == OK) {
                executa_comando(token);
            } else {
                printf("[CloudShell] Não tem saldo para executar comandos.\n)");
            }
        }
    }
}
/*
 * Função responsável pelo envio de um pedido do tipo VERIFICA_SALDO e de receber
 * a resposta no pipe 
 */
static void verifica_saldo() {

    int retorno_read, retorno_write;
    int fd_leitura, fd_escrita;
    AP_PEDIDO_SERVIDOR pedido = (AP_PEDIDO_SERVIDOR) malloc(sizeof (PEDIDO_SERVIDOR));
    AP_RESPOSTA_SERVIDOR resposta = (AP_RESPOSTA_SERVIDOR) malloc(sizeof (RESPOSTA_SERVIDOR));
    pedido->pid_cliente = pid_shell;
    pedido->pid_exec = -1;
    pedido->tipo_pedido = VERIFICA_SALDO;

    fd_escrita = open("Pipes/pedido_contabilizacao", O_WRONLY);
    if (fd_escrita == -1) printf("[CloudShell] Erro ao abrir pipe para escrita %s\n", strerror(errno));


    if ((retorno_write = write(fd_escrita, pedido, sizeof (PEDIDO_SERVIDOR))) > 0) {

        printf("[CloudShell] Enviei o pedido com sucesso. "
                "{pid_cliente = %d  pid_exec = %d accao= %d saldo = %f}\n",
                pedido->pid_cliente, pedido->pid_exec, pedido->tipo_pedido, pedido->saldo);

        fd_leitura = open(path_pipe_resposta_verifica_saldo, O_RDONLY);

        if ((retorno_read = read(fd_leitura, resposta, sizeof (RESPOSTA_SERVIDOR))) > 0) {

            printf("[CloudShell] Li a resposta com sucesso. "
                    "{saldo restante = %f resposta = %d}\n",
                    resposta->saldo_restante, resposta->tipo_resposta);

            if (fd_leitura == -1) printf("[CloudShell] Erro ao abrir pipe para leitura %s\n", strerror(errno));

        }
    }
    close(fd_leitura);
    close(fd_escrita);
}

static AP_RESPOSTA_SERVIDOR adiciona_saldo(double saldo_a_adicionar) {

    int retorno_read, retorno_write;
    int fd_leitura, fd_escrita;
    AP_PEDIDO_SERVIDOR pedido = (AP_PEDIDO_SERVIDOR) malloc(sizeof (PEDIDO_SERVIDOR));
    AP_RESPOSTA_SERVIDOR resposta = (AP_RESPOSTA_SERVIDOR) malloc(sizeof (RESPOSTA_SERVIDOR));
    pedido->pid_cliente = pid_shell;
    pedido->pid_exec = -1;
    pedido->tipo_pedido = ADD_SALDO;
    pedido->saldo = saldo_a_adicionar;

    fd_escrita = open("Pipes/pedido_contabilizacao", O_WRONLY);
    if (fd_escrita == -1) printf("[CloudShell] Erro ao abrir pipe para escrita %s\n", strerror(errno));


    if ((retorno_write = write(fd_escrita, pedido, sizeof (PEDIDO_SERVIDOR))) > 0) {

        printf("[CloudShell] Enviei o pedido com sucesso. "
                "{pid_cliente = %d  pid_exec = %d accao= %d saldo = %f}\n",
                pedido->pid_cliente, pedido->pid_exec, pedido->tipo_pedido, pedido->saldo);

        fd_leitura = open(path_pipe_resposta_addsaldo, O_RDONLY);

        if ((retorno_read = read(fd_leitura, resposta, sizeof (RESPOSTA_SERVIDOR))) > 0) {

            printf("[CloudShell] Li a resposta com sucesso. "
                    "{saldo restante = %f resposta = %d}\n",
                    resposta->saldo_restante, resposta->tipo_resposta);

            if (fd_leitura == -1) printf("[CloudShell] Erro ao abrir pipe para leitura %s\n", strerror(errno));

        }
    }
    close(fd_leitura);
    close(fd_escrita);
    return resposta;
}

static void adiciona_user() {

    int retorno_read, retorno_write;
    int fd_leitura, fd_escrita;
    AP_PEDIDO_SERVIDOR pedido = (AP_PEDIDO_SERVIDOR) malloc(sizeof (PEDIDO_SERVIDOR));
    AP_RESPOSTA_SERVIDOR resposta = (AP_RESPOSTA_SERVIDOR) malloc(sizeof (RESPOSTA_SERVIDOR));
    pedido->pid_cliente = pid_shell;
    pedido->tipo_pedido = ADD_USER;

    fd_escrita = open("Pipes/pedido_contabilizacao", O_WRONLY);
    if (fd_escrita == -1) printf("[CloudShell] Erro ao abrir pipe para escrita %s\n", strerror(errno));


    if ((retorno_write = write(fd_escrita, pedido, sizeof (PEDIDO_SERVIDOR))) > 0) {

        printf("[CloudShell] Enviei o pedido com sucesso. "
                "{pid_cliente = %d  pid_exec = %d accao= %d saldo = %f}\n",
                pedido->pid_cliente, pedido->pid_exec, pedido->tipo_pedido, pedido->saldo);

        fd_leitura = open(path_pipe_resposta_adduser, O_RDONLY);

        if ((retorno_read = read(fd_leitura, resposta, sizeof (RESPOSTA_SERVIDOR))) > 0) {

            printf("[CloudShell] Li a resposta com sucesso. "
                    "{saldo restante = %f resposta = %d}\n",
                    resposta->saldo_restante, resposta->tipo_resposta);

            if (fd_leitura == -1) printf("[CloudShell] Erro ao abrir pipe para leitura %s\n", strerror(errno));

        }
    }
    close(fd_leitura);
    close(fd_escrita);
}

static void executa_comando(char *linha) {
    pid_t retorno_wait;
    pid_t pid_filho_local;
    int estado;
    char **comandos = cmdsLinhaToArray(linha);
    int numero_comandos = length_array(comandos);
    pid_filho_local = fork();


    if (getpid() != pid_shell) {
        if (numero_comandos == 1) {
            auxiliar_executa_1_comando(comandos[0]);
        } else {
            auxiliar_executa_pipes(comandos);
        }
    } else {
        pid_filho_shell = pid_filho_local;
        alarm(1);
        retorno_wait = wait(&estado);
        if (retorno_wait == pid_filho_shell) {
            pid_filho_shell = -1;
        }
    }
}

static void auxiliar_executa_1_comando(char *linha) {
    char **comando_args = argsToArray(linha);
    execvp(comando_args[0], comando_args);
    printf("[BashCliente] Erro no exec. %s\n", strerror(errno));
    _exit(-1);
}

static void auxiliar_executa_pipes(char **comandos) {
    int i = 0;
    int n_ordem = 0;
    char **argumentos;
    pid_t pai = getpid();
    pid_t pid_fork;
    int num_comandos = length_array(comandos);
    int **pn = criaMatriz(num_comandos - 1, 2);

    /*
     * Cria logo de inicio todos os pipes que vão ser necessarios.
     * O que implica que depois cada processo tenha que os fechar a todos,
     * depois de fazer os respectivos dup's. Para fechar todos os pipes, foi criada
     * uma função auxiliar fecha_todos_pipes();
     */
    for (i = 0; i < num_comandos - 1; i++) {
        if (pipe(pn[i]) == -1) {
            printf("[CloudShell] O pipe de ordem %d não foi criado com sucesso. %s\n", i, strerror(errno));
            return;
        }
    }

    if (fork() == 0) {
        /*
         * O filho executa o 1º comando.
         */
        dup2(pn[0][1], 1);
        fecha_todos_pipes(pn, num_comandos);
        argumentos = argsToArray(comandos[0]);
        execvp(argumentos[0], argumentos);
    } else {

        /*
         * Ciclo que cria o número de filhos necessários para correr os comandos.
         * Os filhos têm todos o mesmo pai, correm por isso em paralelo.
         */
        for (i = 1; i < num_comandos - 1; i++) {
            if (getpid() == pai) {
                /*
                 * Só o pai cria filhos.
                 */
                pid_fork = fork();
                if (pid_fork != pai)
                    /*
                     * Cada filho, logo depois de ser criado fica com uma etiqueta
                     * "n_ordem" que indica o comando que ele vai correr. O filho com
                     * n_ordem =1 vai correr o 2º comando, n_ordem = 2 corre o 3º etc...
                     */
                    n_ordem = i;
            }
        }


        if (getpid() != pai) {
            /*
             * Se o processo é um dos filhos criados pelo ciclo anterior, então
             * corre o programa correspondente ao seu nº de ordem.
             */
            dup2(pn[n_ordem - 1][0], 0);
            dup2(pn[n_ordem][1], 1);
            fecha_todos_pipes(pn, num_comandos);
            argumentos = argsToArray(comandos[n_ordem]);
            execvp(argumentos[0], argumentos);
        } else {
            /*
             * O pai corre o último comando.
             */
            dup2(pn[num_comandos - 2][0], 0);
            fecha_todos_pipes(pn, num_comandos);
            argumentos = argsToArray(comandos[num_comandos - 1]);
            execvp(argumentos[0], argumentos);
        }
    }
}

/*
 * Função auxiliar 
 */
static void fecha_todos_pipes(int **fd_pipes, int num_comandos) {
    int i;
    for (i = 0; i < num_comandos - 1; i++) {
        close(fd_pipes[i][0]);
        close(fd_pipes[i][1]);
    }
}

/*
 * Aloca espaço para uma matriz de inteiros de dimensao linhas * colunas.
 * Útil para criação de matrizes que contêm descritores de pipes.
 */
static int**criaMatriz(int linhas, int colunas) {
    int i;
    int **resultado = (int **) malloc(sizeof (int *)*linhas);

    for (i = 0; i < linhas; i++) {
        resultado[i] = (int *) malloc(sizeof (int)*colunas);
    }

    return resultado;
}

static void envia_pedido_servidor(int sig) {
    if (pid_filho_shell != -1) {
        int retorno_read, retorno_write;
        int fd_leitura, fd_escrita;
        AP_PEDIDO_SERVIDOR pedido = (AP_PEDIDO_SERVIDOR) malloc(sizeof (PEDIDO_SERVIDOR));
        AP_RESPOSTA_SERVIDOR resposta = (AP_RESPOSTA_SERVIDOR) malloc(sizeof (RESPOSTA_SERVIDOR));
        pedido->pid_cliente = pid_shell;
        pedido->pid_exec = pid_filho_shell;
        pedido->tipo_pedido = EXECUTE;
        pedido->saldo = -1;

        fd_escrita = open("Pipes/pedido_contabilizacao", O_WRONLY);
        if (fd_escrita == -1) printf("[CloudShell] Erro ao abrir pipe para escrita %s\n", strerror(errno));


        if ((retorno_write = write(fd_escrita, pedido, sizeof (PEDIDO_SERVIDOR))) > 0) {

            printf("[CloudShell] Enviei o pedido com sucesso. "
                    "{pid_cliente = %d  pid_exec = %d accao= %d saldo = %f}\n",
                    pedido->pid_cliente, pedido->pid_exec, pedido->tipo_pedido, pedido->saldo);

            fd_leitura = open(path_pipe_resposta_shell, O_RDONLY);

            if ((retorno_read = read(fd_leitura, resposta, sizeof (RESPOSTA_SERVIDOR))) > 0) {

                printf("[CloudShell] Li a resposta com sucesso. "
                        "{saldo restante = %f resposta = %d}\n",
                        resposta->saldo_restante, resposta->tipo_resposta);

                if (fd_leitura == -1) printf("[CloudShell] Erro ao abrir pipe para leitura %s\n", strerror(errno));

                if (resposta->tipo_resposta == NOT_OK) {
                    kill(pid_filho_shell, SIGKILL);
                }
            }
        }
        close(fd_leitura);
        close(fd_escrita);
        alarm(1);
    }
}

static void cria_pipes_shell() {
    int retorno_mkfifo;
    char *path_pipe_pedido_contabilizacao = "Pipes/pedido_contabilizacao";

    retorno_mkfifo = mkfifo(path_pipe_pedido_contabilizacao, 0666);
    if (retorno_mkfifo == -1)
        printf("Pipe \"%s\" não criado. %s\n", path_pipe_pedido_contabilizacao, strerror(errno));

    sprintf(path_pipe_resposta_shell, "Pipes/%d", pid_shell);
    retorno_mkfifo = mkfifo(path_pipe_resposta_shell, 0666);
    if (retorno_mkfifo == -1)
        printf("Pipe \"%s\" não criado. %s\n", path_pipe_resposta_shell, strerror(errno));

    sprintf(path_pipe_resposta_addsaldo, "Pipes/resposta_addsaldo_%d", pid_shell);
    retorno_mkfifo = mkfifo(path_pipe_resposta_addsaldo, 0666);
    if (retorno_mkfifo == -1)
        printf("Pipe \"%s\" não criado. %s\n", path_pipe_resposta_addsaldo, strerror(errno));
    
    sprintf(path_pipe_resposta_verifica_saldo, "Pipes/resposta_verificasaldo_%d", pid_shell);
    retorno_mkfifo = mkfifo(path_pipe_resposta_verifica_saldo, 0666);
    if (retorno_mkfifo == -1)
        printf("Pipe \"%s\" não criado. %s\n", path_pipe_resposta_verifica_saldo, strerror(errno));


    sprintf(path_pipe_resposta_adduser, "Pipes/resposta_adduser_%d", pid_shell);
    retorno_mkfifo = mkfifo(path_pipe_resposta_adduser, 0666);
    if (retorno_mkfifo == -1)
        printf("Pipe \"%s\" não criado. %s\n", path_pipe_resposta_adduser, strerror(errno));
    

}

static int length_array(char ** array) {
    int i = 0, soma = 0;
    while (array[i++] != NULL) soma++;
    return soma;
}

static char **argsToArray(char *linha) {
    int i = 0;
    char *token, *delimitadores = " \n\r";
    char **array = (char **) malloc(sizeof (char *)*MAX_ARGS);
    char *linha_copia = (char *) malloc(sizeof (char)*(strlen(linha) + 1));
    strcpy(linha_copia, linha);

    token = strtok(linha_copia, delimitadores);
    array[i++] = token;

    while ((token = strtok(NULL, delimitadores)) != NULL && i < MAX_ARGS - 1) {
        array[i++] = token;
    }

    array[i] = NULL;
    return array;
}

static char **cmdsLinhaToArray(char *linha) {
    int i = 0;
    char *token, *delimitadores = "|\n\r";
    char **array = (char **) malloc(sizeof (char *)*MAX_ARGS);
    char *linha_copia = (char *) malloc(sizeof (char)*(strlen(linha) + 1));
    strcpy(linha_copia, linha);

    token = strtok(linha_copia, delimitadores);
    array[i++] = token;

    while ((token = strtok(NULL, delimitadores)) != NULL && i < MAX_ARGS - 1) {
        array[i++] = token;
    }

    array[i] = NULL;
    return array;
}

